﻿using Mathematically.AutoEdger.Tests.Specs;

namespace Mathematically.AutoEdger.Domain
{
    public class AutoEdgerAnalyser: IAutoEdgerAnalyser
    {
        private readonly IPlayer _player;

        public AutoEdgerAnalyser(IPlayer player)
        {
            _player = player;
        }
    }
}