﻿using System.IO.Abstractions;
using FluentAssertions;
using Mathematically.AutoEdger.Domain;
using Mathematically.AutoEdger.Infrastructure;
using Xunit;

namespace Mathematically.AutoEdger.Tests.Specs
{
    public class When_go_is_pressed_with_default_configuration : And_app_has_been_started
    {
        [Fact]
        public void AutoEdger_will_play_a_random_rhythm_once()
        {
            // Infrastructure object that actually plays stuff
            IPlayer player = new Player();

            // We need a fake player that just raises the expected events derived from sequence content

            ISequenceFactory sequenceFactory = new SequenceFactory();

            // Collection of rhythm files found
            IPhraseCatalog catalog = new PhraseCatalog(new FileSystem());

            // The main entry point
            // Configure from ui with global options
            // Has commands e.g. Go, Stop, etc.
            IAutoEdger autoEdger = new AutoEdgerController(player, catalog, sequenceFactory);

            // Generate current sequence and send it to the player
            Controller.Go();

            // Receives events from the player
            // -- started sequence
            // -- started file
            // -- ended file
            // -- ended sequence
            IAutoEdgerAnalyser analyser = new AutoEdgerAnalyser(player);

            // Raises event containing ISequence
//            Controller.SequenceStarted
//
//            // The playback session
//            // reports status
//            // raises events
//            // Owned by AEC?
//            // Must be exposed somehow? IENumerable<ISequence> or NowPlaying on AEC?
//            ISequence session = new AutoEdgerSequence();

            true.Should().Be(true);
        }
    }
}
