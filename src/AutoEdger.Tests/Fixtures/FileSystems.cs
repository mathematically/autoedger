using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;

namespace Mathematically.AutoEdger.Tests.Fixtures
{
    public static class FileSystems
    {
        private static readonly IFileSystem _basic = new MockFileSystem(new Dictionary<string, MockFileData>()
        {
            {@".\Phrases\80\1234.mp3", new MockFileData("")},
            {@".\Phrases\80\1-3-.mp3", new MockFileData("")},
            {@".\Phrases\XXX\1-3-.mp3", new MockFileData("")},
            {@".\Phrases\YYY\1234.txt", new MockFileData("")},
            {@".\Phrases\120\1234.mp3", new MockFileData("")},
            {@".\Phrases\120\1-3-.mp3", new MockFileData("")},
        });

        public static IFileSystem Basic
        {
            get { return _basic; }
        }
    }
}