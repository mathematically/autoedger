using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using Mathematically.AutoEdger.Domain;

namespace Mathematically.AutoEdger.Infrastructure
{
    public class PhraseCatalog : IPhraseCatalog
    {
        private const int IgnoreSpeed = -1;

        private readonly IFileSystem _fileSystem;

        private readonly List<IPhrase> _phrases = new List<IPhrase>();
        private readonly List<int> _speeds = new List<int>();

        public PhraseCatalog(IFileSystem fileSystem)
        {
            _fileSystem = fileSystem;

            var directories = _fileSystem.Directory.GetDirectories(AutoEdgerContext.PHRASE_CATALOG_DIRECTORY);

            if (!directories.Any() && !directories.First().EndsWith("Phrases"))
                throw new Exception("No phrase directory " + AutoEdgerContext.PHRASE_CATALOG_DIRECTORY + " found");

            directories.ForEach(LoadPhrases);
        }

        private void LoadPhrases(string directory)
        {
            int speed = ParseSpeed(directory);
            if (speed == IgnoreSpeed)
                return;

            var wavFiles = _fileSystem.Directory.GetFiles(directory, "*.wav");
            var mp3Files = _fileSystem.Directory.GetFiles(directory, "*.mp3");
            var phraseFiles = wavFiles.Concat(mp3Files).ToList();

            if (!phraseFiles.Any())
                return;

            _speeds.Add(speed);
            phraseFiles.ForEach(f => _phrases.Add(new Phrase(speed, f, _fileSystem.Path.GetFileNameWithoutExtension(f))));
        }

        private int ParseSpeed(string s)
        {
            var lastSubDir = s.Split(new [] {'\\'}).Reverse().SkipWhile(string.IsNullOrEmpty).First();

            int speed = 0;
            if (Int32.TryParse(lastSubDir, out speed))
                return speed;

            return IgnoreSpeed;
        }

        public IEnumerable<IPhrase> Phrases
        {
            get { return _phrases; }
        }

        public IEnumerable<int> Speeds
        {
            get { return _speeds; }
        }
    }
}