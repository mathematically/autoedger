﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("AutoEdger.Domain")]
[assembly: AssemblyDescription("AutoEdger domain assembly")]
[assembly: ComVisible(false)]
[assembly: Guid("9df92f7c-342f-4167-be01-81a2f0d7b892")]
