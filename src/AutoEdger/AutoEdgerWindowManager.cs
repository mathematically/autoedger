﻿using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;

namespace Mathematically.AutoEdger
{
    public class AutoEdgerWindowManager : WindowManager
    {
        protected override Window CreateWindow(object rootModel, bool isDialog, object context, IDictionary<string, object> settings)
        {
            var defaultSettings = new Dictionary<string, object>
            {
                {"Title", "Auto Edger v" + Assembly.GetExecutingAssembly().GetName().Version},
                {"WindowStyle", WindowStyle.ToolWindow},
                {"MinWidth", 300},
                {"MinHeight", 300},
            };

            return base.CreateWindow(rootModel, isDialog, context, settings ?? defaultSettings);
        }
    }
}