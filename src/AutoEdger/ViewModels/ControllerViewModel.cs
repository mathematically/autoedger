﻿using Caliburn.Micro;
using Mathematically.AutoEdger.Annotations;
using Mathematically.AutoEdger.Domain;

namespace Mathematically.AutoEdger.ViewModels
{
    [UsedImplicitly]
    public class ControllerViewModel : Screen
    {
        private readonly IWindowManager _windowManager;
        private readonly IAutoEdger _autoEdger;

        public ControllerViewModel(IWindowManager windowManager, IAutoEdger autoEdger)
        {
            _windowManager = windowManager;
            _autoEdger = autoEdger;
        }

        public void Go()
        {
            _autoEdger.Go();
        }
    }
}
