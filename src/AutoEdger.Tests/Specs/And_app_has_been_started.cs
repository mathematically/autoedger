using System.IO.Abstractions;
using Caliburn.Micro;
using Mathematically.AutoEdger.Domain;
using Mathematically.AutoEdger.Infrastructure;
using Mathematically.AutoEdger.ViewModels;
using NSubstitute;

namespace Mathematically.AutoEdger.Tests.Specs
{
    public class And_app_has_been_started
    {
        protected ControllerViewModel Controller;

        protected And_app_has_been_started()
        {
            StartApp();
        }

        private void StartApp()
        {
            IPhraseCatalog catalog = new PhraseCatalog(new FileSystem());
            ISequenceFactory sequenceFactory = new SequenceFactory();

            IAutoEdger autoEdger = new AutoEdgerController(Substitute.For<IPlayer>(), catalog, sequenceFactory);

            Controller = new ControllerViewModel(Substitute.For<IWindowManager>(), autoEdger);
        }      
    }
}