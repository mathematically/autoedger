using System;
using System.Linq;

namespace Mathematically.AutoEdger.Domain
{
    public class AutoEdgerController : IAutoEdger, ISequenceControlSettings, ISequenceFactory
    {
        private readonly Random _rng = new Random((int)DateTime.Now.Ticks);

        private readonly IPlayer _player;
        private readonly IPhraseCatalog _catalog;
        private readonly ISequenceFactory _sequenceFactory;

        public AutoEdgerController(IPlayer player, IPhraseCatalog catalog, ISequenceFactory sequenceFactory)
        {
            _player = player;
            _catalog = catalog;
            _sequenceFactory = sequenceFactory;

            RepeatCount = 2;
            Speed = null;
        }

        public void Go()
        {
            _player.Play(BuildSequence());
        }

        public int RepeatCount { get; private set; }
        public int? Speed { get; private set; }

        public Func<IPhrase> Selector()
        {
            return SelectRandomFrom;
        }

        private IPhrase SelectRandomFrom()
        {
            if (Speed.HasValue)
                return _catalog.Phrases.Where(p => p.BPM == Speed).ElementAt(_rng.Next(0, _catalog.Phrases.Count() - 1));

            return _catalog.Phrases.ElementAt(_rng.Next(1, _catalog.Phrases.Count()));
        }

        public ISequence BuildSequence()
        {
            var sequence = new Sequence();

            var phrase = SelectRandomFrom();
            for (var n = 0; n < RepeatCount; n++)
            {
                sequence.AddElement(phrase);               
            }

            return sequence;
        }
    }
}