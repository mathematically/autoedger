using System;

namespace Mathematically.AutoEdger.Domain
{
    public interface ISequenceControlSettings
    {
        int RepeatCount { get; }
        int? Speed { get; }
        Func<IPhrase> Selector();
    }
}