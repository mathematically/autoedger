namespace Mathematically.AutoEdger.Domain
{
    public interface ISequenceFactory
    {
        ISequence BuildSequence();
    }

    public class SequenceFactory  : ISequenceFactory
    {
        public ISequence BuildSequence()
        {
            return null;
        }
    }
}