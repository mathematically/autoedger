using System;
using System.Collections;
using System.Collections.Generic;

namespace Mathematically.AutoEdger.Domain
{
    public interface IPhrase : IElement
    {
        int BPM { get; }
        string FullFileName { get; }
        string PhraseDisplay { get; }
    }

    public interface IDelay : IElement
    {
        int Seconds { get; }
    }

    public class Delay : IDelay
    {
        public int Seconds { get; private set; }

        public void Perform(IPlayer player)
        {
            throw new NotImplementedException();
        }
    }

    public interface IElement
    {
        void Perform(IPlayer player);
    }

    public interface ISequence
    {
        void AddElement(IElement element);
        IEnumerable<IElement> Elements { get; }
    }
}