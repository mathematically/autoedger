using System.Collections.Generic;
using System.Linq;
using Mathematically.AutoEdger.Domain;
using NAudio.Wave;

namespace Mathematically.AutoEdger.Infrastructure
{
    public class Player : IPlayer
    {
        private WaveOut _waveOut;
        private WaveFileReader _wavReader;
        private Mp3FileReader _mp3Reader;
        private IEnumerable<IElement> _elements;

        public void Play(ISequence sequence)
        {
            _elements = sequence.Elements.ToList();
            PlayNextElement();
        }

        private void PlayNextElement()
        {
            // Sort of a double dispatch arrangement.
            var element = _elements.First();
            element.Perform(this);

            // Move the position in hte ISequence on to the next element
            _elements = _elements.Skip(1);
        }

        public void Perform(Phrase phrase)
        {
            if (phrase.FullFileName.Contains("mp3"))
            {
                CreateMP3Chain(phrase);
                _waveOut.Init(_mp3Reader);
                _waveOut.Play();

            }
            else if (phrase.FullFileName.Contains("wav"))
            {
                CreateWavChain(phrase);
                _waveOut.Init(_wavReader);
                _waveOut.Play();
               
            }
        }

        private void CreateWavChain(Phrase phrase)
        {
            _waveOut = new WaveOut();
            _waveOut.PlaybackStopped += WavPlaybackStopped;
            _wavReader = new WaveFileReader(phrase.FullFileName);
        }

        private void CreateMP3Chain(Phrase phrase)
        {
            _waveOut = new WaveOut();
            _waveOut.PlaybackStopped += MP3PlaybackStopped;
            _mp3Reader = new Mp3FileReader(phrase.FullFileName);
        }

        void MP3PlaybackStopped(object sender, StoppedEventArgs e)
        {
            DisposeAudioChains();
            ContinueSequence();
        }

        private void WavPlaybackStopped(object sender, StoppedEventArgs stoppedEventArgs)
        {
            DisposeAudioChains();
            ContinueSequence();
        }

        private void ContinueSequence()
        {
            if (_elements.Any())
            {
                PlayNextElement();
            }
        }

        private void DisposeAudioChains()
        {
            if (_mp3Reader != null)
                _mp3Reader.Dispose();
            if (_wavReader != null)
                _wavReader.Dispose();
            if (_waveOut != null)
                _waveOut.Dispose();
        }

        public void Perform(Delay delay)
        {
            throw new System.NotImplementedException();
        }
    }
}