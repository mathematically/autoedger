using System.Linq;
using FluentAssertions;
using Mathematically.AutoEdger.Annotations;
using Mathematically.AutoEdger.Domain;
using Mathematically.AutoEdger.Infrastructure;
using Mathematically.AutoEdger.Tests.Fixtures;
using Xunit;

namespace Mathematically.AutoEdger.Tests.UnitTests
{
    public class Rhythm_catalog_should
    {
        private static IPhraseCatalog CreateSUT()
        {
            return new PhraseCatalog(FileSystems.Basic);
        }

        [Fact]
        public void Find_phrase_subdirectories_and_decode_speed()
        {
            var catalog = CreateSUT();

            catalog.Speeds.Count().Should().Be(2);
            catalog.Speeds.ElementAt(0).Should().Be(80);
            catalog.Speeds.ElementAt(1).Should().Be(120);
        }

        [Fact]
        public void Load_phrases_from_speed_subdirectories()
        {
            var catalog = CreateSUT();

            catalog.Phrases.Count().Should().Be(4);

            catalog.Phrases.Count(p => p.BPM == 80).Should().Be(2);
            catalog.Phrases.Where(p => p.BPM == 80).ElementAt(0).FullFileName.Should().Contain(@"1234.mp3");
            catalog.Phrases.Where(p => p.BPM == 80).ElementAt(0).PhraseDisplay.Should().Be("1234");
            catalog.Phrases.Where(p => p.BPM == 80).ElementAt(1).FullFileName.Should().Contain(@"1-3-.mp3");
            catalog.Phrases.Where(p => p.BPM == 80).ElementAt(1).PhraseDisplay.Should().Be("1-3-");

            catalog.Phrases.Count(p => p.BPM == 120).Should().Be(2);
            catalog.Phrases.Where(p => p.BPM == 120).ElementAt(0).FullFileName.Should().Contain(@"1234.mp3");
            catalog.Phrases.Where(p => p.BPM == 120).ElementAt(1).FullFileName.Should().Contain(@"1-3-.mp3");
        }
    }
}