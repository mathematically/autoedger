﻿using System.Collections.Generic;

namespace Mathematically.AutoEdger.Domain
{
    public interface IPlayer
    {
        void Play(ISequence sequence);
        void Perform(Phrase phrase);
        void Perform(Delay delay);
    }

    public class Sequence : ISequence
    {
        private readonly List<IElement> _elements = new List<IElement>();

        public void AddElement(IElement element)
        {
            _elements.Add(element);
        }

        public IEnumerable<IElement> Elements
        {
            get { return _elements; }
        }
    }
}