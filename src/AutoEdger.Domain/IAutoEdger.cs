using System.Security.Cryptography.X509Certificates;

namespace Mathematically.AutoEdger.Domain
{
    public interface IAutoEdger
    {
        void Go();
    }
}