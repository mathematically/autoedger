using System.Collections.Generic;

namespace Mathematically.AutoEdger.Domain
{
    public interface IPhraseCatalog
    {
        IEnumerable<IPhrase> Phrases { get; }
        IEnumerable<int> Speeds { get; }
    }
}