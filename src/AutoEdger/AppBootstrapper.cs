﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Caliburn.Micro.Logging.NLog;
using Mathematically.AutoEdger.Domain;
using Mathematically.AutoEdger.Infrastructure;
using Mathematically.AutoEdger.Tests.Specs;
using Mathematically.AutoEdger.ViewModels;
using LogManager = Caliburn.Micro.LogManager;

namespace Mathematically.AutoEdger
{
    public class AppBootstrapper: BootstrapperBase
    {
        private StructureMap.IContainer _container;

        static AppBootstrapper()
        {
            LogManager.GetLog = type =>
            {
                if (type.AssemblyQualifiedName.Contains("AutoEdger"))
                    return new NLogLogger(type);

                return new DebugLog(type);
            };
        }

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _container = new StructureMap.Container(x =>
            {
                // Caliburn's window manager so we can specify window options.
                x.For<IWindowManager>().Singleton().Use<AutoEdgerWindowManager>();

                // The composition root for the domain
                x.For<IAutoEdger>().Singleton().Use<AutoEdgerController>();
                x.For<IAutoEdgerAnalyser>().Singleton().Use<AutoEdgerAnalyser>();

                x.For<IPhraseCatalog>().Singleton().Use<PhraseCatalog>();
                x.For<IPlayer>().Singleton().Use<Player>();
                x.For<ISequenceFactory>().Singleton().Use<SequenceFactory>();

                x.For<IFileSystem>().Singleton().Use<FileSystem>();
//                x.For<IItemTextSource>().Use<ClipboardItemTextSource>();
//                x.For<IItemTextChecker>().Use<ItemTextChecker>();
//                x.For<IPoeItemFactory>().Use<PoeItemFactory>();
//                x.For<IPoeItemParser>().Use<PoeItemParser>();
//
//                // Infrastructure objects.
//                x.For<IClipboardMonitor>().Singleton().Use<ClipboardMonitor>();
            });
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            return string.IsNullOrEmpty(key)
                ? _container.GetInstance(serviceType)
                : _container.GetInstance(serviceType ?? typeof (object), key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType).Cast<object>();
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            base.OnStartup(sender, e);
            DisplayRootViewFor<ControllerViewModel>();
        }
    }
}
