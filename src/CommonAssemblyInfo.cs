﻿using System.Reflection;

[assembly: AssemblyProduct("AutoEdger")]
[assembly: AssemblyCopyright("Copyright 2014 https://bitbucket.org/autoedger")]
[assembly: AssemblyTrademark("")]

// 0.8 is an alpha, 0.9 is a beta, 1.0 is release.
// Also see http://semver.org/
[assembly: AssemblyVersion("0.8.0.0")]
[assembly: AssemblyFileVersion("0.8.0.0")]

