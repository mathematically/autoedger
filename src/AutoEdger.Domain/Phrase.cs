using System;

namespace Mathematically.AutoEdger.Domain
{
    public class Phrase : IPhrase
    {
        public int BPM { get; private set; }
        public string FullFileName { get; private set; }
        public string PhraseDisplay { get; private set; }

        public Phrase(int bpm, string fullFileName, string phraseDisplay)
        {
            BPM = bpm;
            FullFileName = fullFileName;
            PhraseDisplay = phraseDisplay;
        }

        public void Perform(IPlayer player)
        {
            player.Perform(this);
        }
    }
}