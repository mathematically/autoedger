﻿using System.Windows.Controls;

namespace Mathematically.AutoEdger.Views
{
    /// <summary>
    /// Interaction logic for ControllerView.xaml
    /// </summary>
    public partial class ControllerView : UserControl
    {
        public ControllerView()
        {
            InitializeComponent();
        }
    }
}
